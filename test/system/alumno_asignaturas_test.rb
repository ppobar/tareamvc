require "application_system_test_case"

class AlumnoAsignaturasTest < ApplicationSystemTestCase
  setup do
    @alumno_asignatura = alumno_asignaturas(:one)
  end

  test "visiting the index" do
    visit alumno_asignaturas_url
    assert_selector "h1", text: "Alumno Asignaturas"
  end

  test "creating a Alumno asignatura" do
    visit alumno_asignaturas_url
    click_on "New Alumno Asignatura"

    fill_in "Alumno", with: @alumno_asignatura.alumno_id
    fill_in "Asignatura", with: @alumno_asignatura.asignatura_id
    fill_in "Fecha publicacion", with: @alumno_asignatura.fecha_publicacion
    fill_in "Nota", with: @alumno_asignatura.nota
    fill_in "Ponderacion", with: @alumno_asignatura.ponderacion
    click_on "Create Alumno asignatura"

    assert_text "Alumno asignatura was successfully created"
    click_on "Back"
  end

  test "updating a Alumno asignatura" do
    visit alumno_asignaturas_url
    click_on "Edit", match: :first

    fill_in "Alumno", with: @alumno_asignatura.alumno_id
    fill_in "Asignatura", with: @alumno_asignatura.asignatura_id
    fill_in "Fecha publicacion", with: @alumno_asignatura.fecha_publicacion
    fill_in "Nota", with: @alumno_asignatura.nota
    fill_in "Ponderacion", with: @alumno_asignatura.ponderacion
    click_on "Update Alumno asignatura"

    assert_text "Alumno asignatura was successfully updated"
    click_on "Back"
  end

  test "destroying a Alumno asignatura" do
    visit alumno_asignaturas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Alumno asignatura was successfully destroyed"
  end
end
