Rails.application.routes.draw do
  resources :horarios
  resources :alumno_asignaturas
  resources :salas
  resources :asignaturas
  resources :alumnos
  resources :profesors
  resources :usuarios
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
