class CreateAlumnoAsignaturas < ActiveRecord::Migration[5.2]
  def change
    create_table :alumno_asignaturas do |t|
      t.float :nota
      t.float :ponderacion
      t.date :fecha_publicacion
      t.references :asignatura, foreign_key: true
      t.references :alumno, foreign_key: true

      t.timestamps
    end
  end
end
