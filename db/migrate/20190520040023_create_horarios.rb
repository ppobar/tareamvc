class CreateHorarios < ActiveRecord::Migration[5.2]
  def change
    create_table :horarios do |t|
      t.string :programa
      t.string :dia
      t.string :bloque_horario
      t.float :hora_inicio
      t.float :hora_fin
      t.references :asignatura, foreign_key: true
      t.references :sala, foreign_key: true

      t.timestamps
    end
  end
end
