class CreateSalas < ActiveRecord::Migration[5.2]
  def change
    create_table :salas do |t|
      t.string :edificio
      t.integer :piso
      t.integer :nro_sala
      t.integer :capacidad
      t.string :tipo

      t.timestamps
    end
  end
end
