class AlumnoAsignaturasController < ApplicationController
  before_action :set_alumno_asignatura, only: [:show, :edit, :update, :destroy]

  # GET /alumno_asignaturas
  # GET /alumno_asignaturas.json
  def index
    @alumno_asignaturas = AlumnoAsignatura.all
  end

  # GET /alumno_asignaturas/1
  # GET /alumno_asignaturas/1.json
  def show
  end

  # GET /alumno_asignaturas/new
  def new
    @alumno_asignatura = AlumnoAsignatura.new
  end

  # GET /alumno_asignaturas/1/edit
  def edit
  end

  # POST /alumno_asignaturas
  # POST /alumno_asignaturas.json
  def create
    @alumno_asignatura = AlumnoAsignatura.new(alumno_asignatura_params)

    respond_to do |format|
      if @alumno_asignatura.save
        format.html { redirect_to @alumno_asignatura, notice: 'Alumno asignatura was successfully created.' }
        format.json { render :show, status: :created, location: @alumno_asignatura }
      else
        format.html { render :new }
        format.json { render json: @alumno_asignatura.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /alumno_asignaturas/1
  # PATCH/PUT /alumno_asignaturas/1.json
  def update
    respond_to do |format|
      if @alumno_asignatura.update(alumno_asignatura_params)
        format.html { redirect_to @alumno_asignatura, notice: 'Alumno asignatura was successfully updated.' }
        format.json { render :show, status: :ok, location: @alumno_asignatura }
      else
        format.html { render :edit }
        format.json { render json: @alumno_asignatura.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alumno_asignaturas/1
  # DELETE /alumno_asignaturas/1.json
  def destroy
    @alumno_asignatura.destroy
    respond_to do |format|
      format.html { redirect_to alumno_asignaturas_url, notice: 'Alumno asignatura was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_alumno_asignatura
      @alumno_asignatura = AlumnoAsignatura.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def alumno_asignatura_params
      params.require(:alumno_asignatura).permit(:nota, :ponderacion, :fecha_publicacion, :asignatura_id, :alumno_id)
    end
end
