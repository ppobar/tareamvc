json.extract! horario, :id, :programa, :dia, :bloque_horario, :hora_inicio, :hora_fin, :asignatura_id, :sala_id, :created_at, :updated_at
json.url horario_url(horario, format: :json)
