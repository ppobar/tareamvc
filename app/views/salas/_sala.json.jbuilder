json.extract! sala, :id, :edificio, :piso, :nro_sala, :capacidad, :tipo, :created_at, :updated_at
json.url sala_url(sala, format: :json)
