json.extract! alumno_asignatura, :id, :nota, :ponderacion, :fecha_publicacion, :asignatura_id, :alumno_id, :created_at, :updated_at
json.url alumno_asignatura_url(alumno_asignatura, format: :json)
